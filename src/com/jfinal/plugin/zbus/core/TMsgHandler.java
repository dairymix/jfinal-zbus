/**
 * Copyright (c) 2015, 玛雅牛［李飞］ (lifei@wellbole.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jfinal.plugin.zbus.core;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.TableMapping;
import com.jfinal.plugin.zbus.coder.Coder;
import com.jfinal.plugin.zbus.coder.JsonCoder;
import com.jfinal.plugin.zbus.kit.ZbusKit;
import org.zbus.mq.Consumer.ConsumerHandler;
import org.zbus.net.http.Message;
import org.zbus.net.http.Message.MessageHandler;

import java.io.IOException;

/**
 * @ClassName: TMsgHandler
 * @Description: 泛型消息回调接口（自动转型）
 * @author 李飞 (lifei@wellbole.com)
 * @date 2015年8月2日 上午1:27:46
 * @since V1.0.0
 */
public abstract class TMsgHandler<T> implements MessageHandler,ConsumerHandler {

	/**
	 * 范型类型
	 */
	private Class<?> tClass;

	/**
	 * 编码解码器
	 */
	private static final Coder coder = new JsonCoder();

	/**
	 * <p>
	 * Title: TMsgHandler
	 * </p>
	 * <p>
	 * Description: 构造函数
	 * </p>
	 * 
	 * @since V1.0.0
	 */
	public TMsgHandler() {
		tClass = getSuperClassGenricType();
	}

	/**
	 * @Title: handle
	 * @Description: 消费者收到消息后的处理函数，子类需实现此方法
	 * @param msg
	 *            收到的消息
	 * @since V1.0.0
	 */
	public abstract void handle(T msg);

	protected void tClassReload(){
        if (Model.class.isAssignableFrom(tClass)) {
            tClass = TableMapping.me().findTableMap(tClass.getName()).getKey();
        }
	}

    @SuppressWarnings("unchecked")
    protected T messageToModel(Message msg) throws IOException {
        Object obj;
        try {
            obj = coder.decode(tClass, msg);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
        //this.handle((T) obj);
        return (T) obj;
    }

	private Class getSuperClassGenricType() {
		return ZbusKit.getSuperClassGenricType(getClass());
	}
}
