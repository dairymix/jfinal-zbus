/** 
 *
 * Lisense:
 * Copyright (c) 2015.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * @fileDesc:TODO
 * @author:dairymix
 * @version:1.0.0
 */
package com.jfinal.plugin.zbus.type;
/** 
 * @ClassName:MsgType
 * @Description:消息类型区别
 * @author:dairymix
 * @version:1.0.0
 */
public enum SendType {
    sync("同步","发送同步类型的消息"), 
    async("异步","发送异步类型的消息"),
    ;

    public String name;
    public String desc;

    SendType(String name, String desc) {
        this.name = name;
        this.desc = desc;
    }
}
