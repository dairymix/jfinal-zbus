package com.jfinal.plugin.zbus;

import com.jfinal.config.Plugins;
import com.jfinal.kit.Prop;
import com.jfinal.log.Logger;
import com.jfinal.plugin.zbus.core.TMsgHandler;

public class ZbusCfg {

    private static final Logger log = Logger.getLogger(ZbusCfg.class);

    //注意,最先配置的为主数据源,配置项名称可以省略(采用默认值)
    // 配置zbus插件
    /********************构造区**********************/

    //配置上移,根据需要补充构造
    //zbus的其他构造方式可以根据需要补充

    /********************内部方法区**********************/

    /**
     * 默认参数为:
     * brokerAddress,zbusHandlerPackage
     */

    public ZbusCfg init(String brokerAddress, String zbusHandlerPackage,
                        Boolean hasGlobalMaxNum, Integer consumerNum){
        init(new ZbusPlugin(brokerAddress,zbusHandlerPackage,hasGlobalMaxNum,consumerNum));
        return this;
    }

    public ZbusCfg init(String brokerAddress,
                        Boolean hasGlobalMaxNum, Integer consumerNum){
        init(new ZbusPlugin(brokerAddress,hasGlobalMaxNum,consumerNum));
        return this;
    }
//    public ZbusCfg init(BrokerConfig config){
//        init(new ZbusPlugin(config));
//        return this;
//    }
//    public ZbusCfg init(BrokerConfig config, String zbusHandlerPackage){
//        init(new ZbusPlugin(config,zbusHandlerPackage));
//        return this;
//    }

    public ZbusCfg init(ZbusPlugin zbus){
        return setZbus(zbus);
    }

    public ZbusCfg init(Prop prop){
        String brokerAddress = prop.get("brokerAddress");
        String zbusHandlerPackage = prop.get("zbusHandlerPackage");
        if (zbusHandlerPackage != null && !zbusHandlerPackage.isEmpty()){
            if (prop.containsKey("maxConsumerNum")){
                init(brokerAddress,zbusHandlerPackage,true,prop.getInt("maxConsumerNum"));
            } else {
                init(brokerAddress,zbusHandlerPackage,false,1);
            }
        } else {
            if (prop.containsKey("maxConsumerNum")){
                init(brokerAddress,true,prop.getInt("maxConsumerNum"));
            } else {
                init(brokerAddress,false,1);
            }
        }

        return this;
    }

//    public ZbusCfg init(){
//        return init(this.prop);
//    }

    public ZbusCfg load(){
        getZbus().load();
        return this;
    }

    public ZbusCfg set(Plugins me) {
        me.add(getZbus());
        return this;
    }

    public ZbusCfg start() {
        if(getZbus().start()) log.debug("start ZbusPlugin ...");
        return this;
    }

    /********************属性方法区**********************/

    public ZbusPlugin getZbus() {
        return zbus;
    }

    public ZbusCfg setZbus(ZbusPlugin zbus) {
        this.zbus = zbus;
        return this;
    }

    //增加链式注册方法,便于非扫描状态下手工注册消息队列(queue)
    public ZbusCfg registerQueue(String consumer, Integer consumerNum, TMsgHandler<?> msgHandler) {
        //this.zbus.registerMqMsgHandler(consumer,msgHandler);
        this.zbus.adaptor().queue.receiverQ.register(consumer,consumerNum,msgHandler);
        return this;
    }

    //增加链式注册方法,便于非扫描状态下手工注册消息队列(topic)
    public ZbusCfg registerTopic(String mq, String topic, Integer consumerNum, TMsgHandler<?> msgHandler) {
        //this.zbus.registerTopicMsgHandler(mq,topic,msgHandler);
        this.zbus.adaptor().topic.receiverT.register(mq,topic,consumerNum,msgHandler);
        return this;
    }

    private ZbusPlugin zbus;
    //private Prop prop = PropCfg.zbus();

    //如果后续需要使用多连接池,可以在这里进行扩充

}
