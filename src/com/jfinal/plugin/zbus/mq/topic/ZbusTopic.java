/**
 * Copyright (c) 2015, 玛雅牛［李飞］ (lifei@wellbole.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jfinal.plugin.zbus.mq.topic;

import com.jfinal.log.Logger;
import com.jfinal.plugin.zbus.core.AbstractSender;
import com.jfinal.plugin.zbus.core.TMsgHandler;
import org.reflections.Reflections;
import org.zbus.mq.Protocol;
import org.zbus.net.http.Message;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @ClassName: ZbusTopic
 * @Description: Topic主题对象  
 * @since V1.0.0
 */
public class ZbusTopic {

    private static final Logger log = Logger.getLogger(ZbusTopic.class);

    public String name;
	public String topic;

    public ReceiverT receiverT = new ReceiverT();
    public SenderT senderT;

	public ZbusTopic init(String name, String topic){
        this.name = name;
        this.topic = topic;
        this.senderT = new SenderT(name,topic);
        return this;
    }

    /**
     * @ClassName: RegT
     * @Description: Topic注解注册
     * @since V1.0.0
     */
    @Retention(RetentionPolicy.RUNTIME)
    @Target({ ElementType.TYPE })
    public @interface RegT{
        String name();
        String topic();
        boolean enable() default true;
    }

    /**
     * @ClassName: SenderT
     * @Description: Topic泛型发送器
     * @since V1.0.0
     */
    public class SenderT<T> extends AbstractSender<T> {

        private final String inner_topic;

        /**
         * @ClassName: SenderT
         * @Description: 构建一个Topic发送器
         * @since V1.0.0
         */
        public SenderT() {
            this(name, topic);
        }

        /**
         * @ClassName: SenderT
         * @Description: 构建一个Topic发送器
         * @since V1.0.0
         */
        public SenderT(String mqName, String topic) {
            super(mqName, Protocol.MqMode.PubSub);
            this.inner_topic = topic;
        }

        //设定topic
        @Override
        protected Message encode(T obj) {
            return super.encode(obj).setTopic(this.inner_topic);
        }
    }

    public static Map<String, Map<String, TMsgHandler<?>>> tMsgHandlerTMap = new HashMap<String, Map<String, TMsgHandler<?>>>();

    public class ReceiverT {

        /**
         * @ClassName: register
         * @Description: 注册Topic的消息回调接口
         * @since V1.0.0
         */
        public ReceiverT register(TMsgHandler<?> msgHandler) {
            return this.register(name, topic, msgHandler);
        }

        /**
         * @ClassName: register
         * @Description: 注册Topic的消息回调接口
         * @since V1.0.0
         */
        public ReceiverT register(String mqName, String topic, TMsgHandler<?> msgHandler) {
            // 依据mq获得 topic－TMsgHandler映射map
            Map<String, TMsgHandler<?>> tmc = tMsgHandlerTMap.get(mqName);
            if (null == tmc) {
                tmc = new HashMap<String, TMsgHandler<?>>();
            }
            if(tmc.containsKey(topic)){
                log.warn("(mq=" + mqName + ",topic=" + topic + ")对应的消息处理器已存在!");
            }
            tmc.put(topic, msgHandler);
            tMsgHandlerTMap.put(mqName, tmc);
            return this;
        }

        public ReceiverT autoLoadTAnno(Reflections reflections){
            Set<Class<?>> topicHandlerClasses = reflections.getTypesAnnotatedWith(RegT.class);
            for (Class<?> mc : topicHandlerClasses) {
                if(!TMsgHandler.class.isAssignableFrom(mc)){
                    throw new RuntimeException(mc.getName() + " 必须继承自 TMsgHandler<T>");
                }
                RegT th = mc.getAnnotation(RegT.class);
                try {
                    if(th.enable()) {
                        TMsgHandler<?> hander = (TMsgHandler<?>) mc.newInstance();
                        register(th.name(), th.topic(), hander);
                        log.info("通过注解自动加载Topic消息处理器( mq=" + th.name()  + ",topic=" + th.topic() + ",handler=" + mc.getName() + " )");
                    }
                } catch (Exception e) {
                    throw new RuntimeException(e.getMessage(),e);
                }
            }
            return this;
        }

    }

}
